// When document is rendered jQuery function start
// import { offset } from 'dom7/dist/dom7.modular';
$(document).ready(function() {
    // <!-- Initialize Swiper -->
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        }
    });

    // Click Event
    $('.main-nav a, .mobile-nav a').click(function() {
        // Current Clicked Link
        var self = $(this);

        // Taking Value of href attribute
        var target = self.attr('href');

        // console.log(target);

        //Animate jQuery Object $(target)
        //offset() return coordinates of selected element
        $('html, body').animate(
            {
                scrollTop: $(target).offset().top - 61
            },
            1000
        );
    });

    $('.mobile-btn').click(function() {
        $('.mobile-nav').toggleClass('active');
    });
});
